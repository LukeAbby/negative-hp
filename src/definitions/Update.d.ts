type IsObject<T> = T extends Array<any> ? false : T extends object ? true : false;

type ObjectKeys<T> = {
	[K in keyof T]: IsObject<T[K]> extends true ? K : never;
}[keyof T];

type ObjectValues<T> = T[ObjectKeys<T>];

type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (k: infer I) => void ? I : never;

type DotNotation<T, Prefix extends string = ''> = IsObject<T> extends true ? {
    [K in keyof T & string as `${Prefix}${K}`]: IsObject<T[K]> extends true ? DotNotation<T[K], `${Prefix}${K}.`> : T[K];
} : T;

type Flatten<T> = IsObject<T> extends true ? Omit<T, ObjectKeys<T>> & UnionToIntersection<ObjectValues<T>> : T;
type DeepFlatten<T> = Flatten<Flatten<Flatten<Flatten<Flatten<T>>>>>;

type Update<T> = Partial<DeepFlatten<DotNotation<T>>>;
