type Ability5e = {
	value: number;
	proficient: 0 | 0.5 | 1;
};

type CommonData5e = {
	abilities: {
		str: Ability5e;
		dex: Ability5e;
		con: Ability5e;
		int: Ability5e;
		wis: Ability5e;
		cha: Ability5e;
	};
	attributes: {
		ac: {
			value: number;
		};
		hp: {
			value: number | string;
			min: number;
			max: number;
			temp: number | string;
			tempmax: number;
		};
		init: {
			value: number;
			bonus: number;
		};
	};
	details: {
		biography: {
			value: string;
			public: string;
		};
	};
	traits: {
		size: string;
		di: {
			value: string[];
			custom: string;
		};
		dr: {
			value: string[];
			custom: string;
		};
		dv: {
			value: string[];
			custom: string;
		};
		ci: {
			value: string[];
			custom: string;
		};
	};
	currency: {
		pp: number;
		gp: number;
		ep: number;
		sp: number;
		cp: number;
	};
};

type ActorData5e = CommonData5e;

type Options = {};

declare class Actor5e extends Actor<ActorData5e> {
	token: Token5e;

	applyDamage(amount?: number | string, multiplier?: number): Promise<Actor>;

	update(data: Update<this>, options?: Options): Promise<this>;
	update(data: Update<this>[], options?: Options): Promise<this[]>;
}
