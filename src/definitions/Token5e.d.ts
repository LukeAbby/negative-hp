// Some properties are commented out because the overrides are invalid.

declare class Token5e extends Token {
	name: string;
	x: number;
	y: number;
	displayName: number;
	img: string;
	width: number;
	height: number;
	// scale: number;
	elevation: number;
	lockRotation: boolean;
	// rotation: Number;
	effects: string[];
	overlayEffect: string;
	// vision: boolean;
	dimSight: number;
	brightSight: number;
	dimLight: number;
	brightLight: number;
	sightAngle: number;
	hidden: number;
	actorId: string;
	actorLink: true;
	actorData: ActorData5e;
	disposition: number;
	displayBars: number;
	bar1: { attribute: string };
	bar2: { attribute: string };

	update(data: Update<this>, options?: Options): Promise<this>;
	update(data: Update<this>[], options?: Options): Promise<this[]>;
}
