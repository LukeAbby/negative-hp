/**
 * Overrides the rubberbanding of health in D&D 5e core, midi-qol, and Tidy5e to allow negative HP to -maxHP.
 * Author: Luke Abby
 * Content License: OGL
 * Software License: GNU
 */

import { importMidiQoL } from './midi-qol';
import { hijackDeathSaves } from './tidy-5e';

Hooks.once('init', async function () {
	patchDamage();

	// @ts-expect-error
	Token.prototype._drawBar = _drawBar;

	await importMidiQoL();
});

// This is called every time the Tidy5e sheet renders.
Hooks.on('renderTidy5eSheet', hijackDeathSaves);

function patchDamage() {
	log('Patching applyDamage on character sheets.');

	const actor = game.dnd5e.entities.Actor5e;
	actor.prototype.applyDamage = applyDamage;
}

/* Core D&D 5e Support */

// The only logic change is to how health is clamped allowing it to go to -hp.max.
// The rest is copied exactly as found in Foundry and then made compatible with Typescript.
async function applyDamage(this: Actor5e, amount: string | number = 0, multiplier: number = 1) {
	amount = Math.floor(toInteger(amount) * multiplier);

	const hp = this.data.data.attributes.hp;

	// Deduct damage from temp HP first
	const tmp = toInteger(hp.temp) || 0;
	const dt = amount > 0 ? Math.min(tmp, amount) : 0;

	// Remaining goes to health
	const tmpMax = toInteger(hp.tempmax) || 0;

	// const dh = Math.clamped(hp.value - (amount - dt), 0, hp.max + tmpMax);
	const dh = Math.clamped(toInteger(hp.value) - (amount - dt), -hp.max, hp.max + tmpMax);

	// Update the Actor
	const updates: Update<Actor5e> = {
		'data.attributes.hp.temp': tmp - dt,
		'data.attributes.hp.value': dh,
	};

	if (this?.token != null && dh < -hp.max) {
		const tokenUpdates: Update<Token5e> = {
			effects: Array.from(new Set(this.token.effects).add('icons/svg/skull.svg')),
		};
		await this.token.update(tokenUpdates);
	}

	return await this.update(updates);
}

type Data = {
	value: number;
	max: number;
	attribute: string;
};

// A copy and paste of the internal functions for the token bar, modified to draw the bar positive with a red colour.
const _drawBar = function (number: number, bar: PIXI.Graphics, data: Data) {
	log('Running patched _drawBar');

	const val = Number(data.value);
	let pct: number;
	if (data.attribute === 'attributes.hp') {
		pct = Math.clamped(Math.abs(val), 0, data.max) / data.max;
	} else {
		pct = Math.clamped(val, 0, data.max) / data.max;
	}

	let h = Math.max(canvas.dimensions.size / 12, 8);
	if (this.data.height >= 2) h *= 1.6; // Enlarge the bar for large tokens

	// Draw the bar
	let color: number[];
	if (val < 0) {
		color = [1, 0, 0];
	} else {
		color = number === 0 ? [1 - pct / 2, pct, 0] : [0.5 * pct, 0.7 * pct, 0.5 + pct / 2];
	}
	bar.clear()
		.beginFill(0x000000, 0.5)
		.lineStyle(2, 0x000000, 0.9)
		.drawRoundedRect(0, 0, this.w, h, 3)
		.beginFill(PIXI.utils.rgb2hex(color), 0.8)
		.lineStyle(1, 0x000000, 0.8)
		.drawRoundedRect(1, 1, pct * (this.w - 2), h - 2, 2);

	// Set position
	let posY = number === 0 ? this.h - h : 0;
	bar.position.set(0, posY);
};

export function toInteger(n: string | number) {
	if (typeof n === 'string') {
		return parseInt(n);
	}

	return n;
}

export function log(...data: any[]) {
	console.log('negative-hp |', ...data);
}
