/* Tidy 5e Support */

import { toInteger, log } from './negative-hp';

// Tidy5e makes a classname of hp-<currentHP>-<temp>. Where currentHP and temp are integers that may be negative.
const classPattern = /^hp-(-?\d+)-(-?\d+)$/;

export function hijackDeathSaves() {
	$('.tidy5e-sheet .profile').each(function () {
		const classes = this.className.split(' ');
		for (const className of classes) {
			const classMatches = classPattern.exec(className);
			if (classMatches != null && classMatches.length !== 0) {
				// Pretend that the currentHP and tempHP is rubberbanded to 0 as the module expects.
				const currentHP = Math.max(toInteger(classMatches[1]), 0);
				const tempHP = Math.max(toInteger(classMatches[2]), 0);

				const rubberbandClass = `hp-${currentHP}-${tempHP}`;
				if (className !== rubberbandClass) {
					this.classList.remove(className);
					this.classList.add(rubberbandClass);

					log(`Overriding "${className}" to "${rubberbandClass}" in Tidy5e sheet.`);
				}
			}
		}
	});
}
