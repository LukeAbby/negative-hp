/* Midi-QoL support */

import { toInteger, log } from './negative-hp';

// The dynamic imports aren't going to be picked up by Typescript because:
//   1. The folder is compiled into dist and symlinked into the modules folder.
//   2. Midi-QoL might not be installed locally anyways.
// They're made as dynamic imports so we can catch the error.
export async function importMidiQoL() {
	let midiQoL: any;
	let utils: any;
	try {
		// @ts-expect-error
		midiQoL = await import('/modules/midi-qol/midi-qol.js');

		// @ts-expect-error
		utils = await import('/modules/midi-qol/module/utils.js');
	} catch (e) {
		log('Midi-Qol probably not installed:', e);
		return;
	}

	if (utils?.calculateDamage != null) {
		log('Patching Midi-QoL calculateDamage.');
		utils.calculateDamage.prototype = calculateDamageWrapper(utils.debug, utils.log);
	} else {
		log('Could not load calculateDamage from Midi-QoL.');
	}
}

// Slightly refactored for Typescript but mostly changed to fix rubberbanding.
const calculateDamageWrapper = function (debug: (...params: any[]) => void, log: (...params: any[]) => void) {
	return function (actor: Actor5e, appliedDamage: number, token: Token5e, totalDamage: number, dmgType: string) {
		log('Running patched midi-Qol calculateDamage.');

		debug('calculate damage ', actor, appliedDamage, token, totalDamage, dmgType);
		let value = Math.floor(appliedDamage);

		const hp = actor.data.data.attributes.hp;
		let tmp: number;
		let oldHP: number | string;
		let newTemp: number;
		let newHP: number | string;

		if (dmgType.includes('temphp')) {
			// only relevent for healing of tmp HP
			tmp = toInteger(hp.temp) || 0;
			oldHP = hp.value;
			newTemp = Math.max(tmp - value, -hp.max);
			newHP = hp.value;
		} else {
			tmp = toInteger(hp.temp) || 0;
			let dt = value > 0 ? Math.min(tmp, value) : 0;
			newTemp = tmp - dt;
			oldHP = hp.value;
			newHP = Math.clamped(toInteger(hp.value) - (value - dt), -hp.max, hp.max + (toInteger(hp.tempmax) || 0));
		}

		debug('calculateDamage: results are ', newTemp, newHP, appliedDamage, totalDamage);
		if (game.user.isGM) {
			log(`${actor.name} takes ${value} reduced from ${totalDamage} Temp HP ${newTemp} HP ${newHP}`);
		}

		return {
			tokenID: token.id,
			actorID: actor._id,
			tempDamage: tmp - newTemp,
			hpDamage: toInteger(oldHP) - toInteger(newHP),
			oldTempHP: tmp,
			newTempHP: newTemp,
			oldHP: oldHP,
			newHP: newHP,
			totalDamage: totalDamage,
			appliedDamage: value,
		};
	};
};
